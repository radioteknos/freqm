# Pin name action command file

# Start of element RR1
ChangePinName(RR1, 1, 1)
ChangePinName(RR1, 2, 2)

# Start of element R9
ChangePinName(R9, 1, 1)
ChangePinName(R9, 2, 2)

# Start of element R11
ChangePinName(R11, 1, 1)
ChangePinName(R11, 2, 2)

# Start of element CONN2
ChangePinName(CONN2, 6, 6)
ChangePinName(CONN2, 5, 5)
ChangePinName(CONN2, 4, 4)
ChangePinName(CONN2, 3, 3)
ChangePinName(CONN2, 2, 2)
ChangePinName(CONN2, 1, 1)

# Start of element C7
ChangePinName(C7, 2, 2)
ChangePinName(C7, 1, 1)

# Start of element Q2
ChangePinName(Q2, 1, 1)
ChangePinName(Q2, 3, 3)
ChangePinName(Q2, 2, 2)

# Start of element U2
ChangePinName(U2, 6, S)
ChangePinName(U2, 5, VCC)
ChangePinName(U2, 4, A)
ChangePinName(U2, 3, B1)
ChangePinName(U2, 2, GND)
ChangePinName(U2, 1, B2)

# Start of element C5
ChangePinName(C5, 2, 2)
ChangePinName(C5, 1, 1)

# Start of element R8
ChangePinName(R8, 1, 1)
ChangePinName(R8, 2, 2)

# Start of element D1
ChangePinName(D1, 3, 3)
ChangePinName(D1, 2, 2)
ChangePinName(D1, 1, 1)

# Start of element R7
ChangePinName(R7, 1, 1)
ChangePinName(R7, 2, 2)

# Start of element R10
ChangePinName(R10, 1, 1)
ChangePinName(R10, 2, 2)

# Start of element C8
ChangePinName(C8, 2, 2)
ChangePinName(C8, 1, 1)

# Start of element C6
ChangePinName(C6, 2, 2)
ChangePinName(C6, 1, 1)

# Start of element C4
ChangePinName(C4, 2, 2)
ChangePinName(C4, 1, 1)

# Start of element R6
ChangePinName(R6, 1, 1)
ChangePinName(R6, 2, 2)

# Start of element R5
ChangePinName(R5, 1, 1)
ChangePinName(R5, 2, 2)

# Start of element U1
ChangePinName(U1, 6, 6)
ChangePinName(U1, 1, 1)
ChangePinName(U1, 2, 2)
ChangePinName(U1, 5, 5)
ChangePinName(U1, 4, 4)
ChangePinName(U1, 3, 3)

# Start of element C3
ChangePinName(C3, 2, 2)
ChangePinName(C3, 1, 1)

# Start of element R1
ChangePinName(R1, 1, 1)
ChangePinName(R1, 2, 2)

# Start of element C1
ChangePinName(C1, 2, 2)
ChangePinName(C1, 1, 1)

# Start of element R2
ChangePinName(R2, 1, 1)
ChangePinName(R2, 2, 2)

# Start of element R4
ChangePinName(R4, 1, 1)
ChangePinName(R4, 2, 2)

# Start of element C2
ChangePinName(C2, 2, 2)
ChangePinName(C2, 1, 1)

# Start of element R3
ChangePinName(R3, 1, 1)
ChangePinName(R3, 2, 2)

# Start of element Q1
ChangePinName(Q1, 1, 1)
ChangePinName(Q1, 3, 3)
ChangePinName(Q1, 2, 2)

# Start of element CONN1
ChangePinName(CONN1, 1, 1)
ChangePinName(CONN1, 2, 2)

# Start of element U3
ChangePinName(U3, 1, 1)
ChangePinName(U3, 2, 2)
ChangePinName(U3, 4, 4)
ChangePinName(U3, 3, 3)

# Start of element C9
ChangePinName(C9, 2, 2)
ChangePinName(C9, 1, 1)

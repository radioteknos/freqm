/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define CFG_ENABLE		(1 << 0)
#define CFG_MASTER		(1 << 2)
#define CFG_SLAVE		(0 << 2)
#define CFG_LSBF		(1 << 3)
#define CFG_CPHA		(1 << 4)
#define CFG_CPOL		(1 << 5)
#define CFG_MOSIDRV		(1 << 6)
#define CFG_LOOPBACK		(1 << 7)
#define CFG_SPOL(s)		(1 << (8 + s))

#define DLY_PREDELAY(d)		((d) << 0)
#define DLY_POSTDELAY(d)	((d) << 4)
#define DLY_FRAMEDELAY(d)	((d) << 8)
#define DLY_INTERDELAY(d)	((d) << 12)

#define STAT_RXRDY 		(1 << 0)
#define STAT_TXRDY 		(1 << 1)
#define STAT_RXOVERRUN 		(1 << 2)
#define STAT_TXUNDERRUN 	(1 << 3)
#define STAT_SELNASSERT 	(1 << 4)
#define STAT_SELNDEASSERT       (1 << 5)
#define STAT_CLKSTALL 		(1 << 6)
#define STAT_EOF     		(1 << 7)
#define STAT_ERROR_MASK		(STAT_RXOVERRUN|STAT_TXUNDERRUN|STAT_SELNASSERT|STAT_SELNDEASSERT|STAT_CLKSTALL)
#define STAT_MSTIDLE     	(1 << 8)


#define TXDATCTL_SSELN(s) 	(s << 16)
#define TXDATCTL_EOT 		(1 << 20)
#define TXDATCTL_EOF 		(1 << 21)
#define TXDATCTL_RX_IGNORE	(1 << 22)
#define TXDATCTL_FSIZE(s)	((s) << 24)

#define RXDAT_SOT		(1 << 20)


#define ss_low() LPC_GPIO_PORT->CLR0=(1<<SS)
#define ss_high() LPC_GPIO_PORT->SET0=(1<<SS)

void spi0_init(void);
uint32_t spi8_send_get(uint32_t v);

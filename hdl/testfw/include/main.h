/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define BAUDRATE 115200

#define BUFFLEN 32

/* stat bits */
#define tmpflag          2
#define ee_fault         1
#define debug            0


/*
  pins for the rfbctrl v1.0 board (2020-08-04)

    SCK    -  PIO0_2
    SELAB  -  PIO0_17   (MOSI/A_B)
    ADCSS  -  PIO0_1    (SS/CS)
    MISO   -  PIO0_13   (MISO/MO)

*/

#define __HW_VERSION "1.0"
#define __FW_VERSION "1.0"

#define MOSI        17                          /* MOSI/A_B     PIO0_17 */
#define SS           1                          /* SS/CS        PIO0_1  */
#define SCK          2                          /* SPIOSCK      PIO0_2  */
#define MISO        13                          /* MISO/MO      PIO0_13 */


uint32_t cmd_help(char* params __attribute__((unused)));

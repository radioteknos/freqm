/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2019
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */


#define PRESCTASKCNTVAL 10       /* prescaled tasks are executed every 10ms */

#define __MCRATE 50

#define KBD_DEB_TO 15                             /* prescaled unit of time */

#define KBD_B_ADC_INF 635
#define KBD_B_ADC_SUP 777
#define KBD_A_ADC_INF 1166
#define KBD_A_ADC_SUP 1425
#define KBD_ESC_ADC_INF 1734
#define KBD_ESC_ADC_SUP 1996
#define KBD_ENTER_ADC_INF 2289
#define KBD_ENTER_ADC_SUP 2529
#define KBD_DOWN_ADC_INF 2946
#define KBD_DOWN_ADC_SUP 3256
#define KBD_UP_ADC_INF 3399
#define KBD_UP_ADC_SUP 3757

void delay_ms(uint32_t); 
uint8_t kbd_get_key(void);

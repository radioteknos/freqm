/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

/* I2CxCFG */
#define MSTEN        0
#define SLVEN        1
#define MONEN        2
#define TIMEOUTEN    3
#define MONCLKSTR    4

/* I2CxSTAT */
#define MSTPENDING   0x00000001
#define MSTSTATE     0x0000000e
#define MSTARBLOSS   0x00000010
#define MSTSTSTPERR  0x00000040
#define SLVPENDING   0x00000100
#define SLVSTATE     0x00000600
#define SLVNOTSTR    0x00000800
#define SLVIDX       0x00003000
#define SLVSEL       0x00004000
#define SLVDESEL     0x00008000
#define MONRDY       0x00010000
#define MONOV        0x00020000
#define MONACTIVE    0x00040000
#define MONIDLE      0x00080000
#define EVENTTIMEOUT 0x01000000
#define SCLTIMEOUT   0x02000000

#define MSTSTATE_IDLE         (0<<1)
#define MSTSTATE_RXRDY        (1<<1)
#define MSTSTATE_TXRDY        (2<<1)
#define MSTSTATE_NACKADDR     (3<<1)
#define MSTSTATE_NACKDATA     (4<<1)

/* I2CxMSTCTL */
#define MSTCONTINUE  0
#define MSTSTART     1
#define MSTSTOP      2
#define MSTDMA       3


#define i2c0_send_start() LPC_I2C0->MSTCTL=(1<<MSTSTART)
#define i2c0_send_stop() LPC_I2C0->MSTCTL=(1<<MSTSTOP)

/* 7bit device addr */
#define EEADDR        0x50  /* 0xa0 for 24LCxx */


void i2c0_init(void);
uint16_t i2c0_send_start_addr(uint8_t addr), i2c0_send_data(uint8_t v),
  i2c0_receive_data(uint8_t devaddr, uint8_t* v);

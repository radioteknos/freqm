/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define ee_hw_major_ver   0x0          /* hw major version number - uint8_t */
#define ee_hw_minor_ver   0x1          /* hw minor version number - uint8_t */
#define ee_serno          0x2                /* serial number 'xxx' char[3] */
#define ee_fw_major_ver   0x5          /* fw major version number - uint8_t */
#define ee_fw_minor_ver   0x6          /* fw minor version number - uint8_t */

#define ee_fmeas          0x10          /* ch A measurement freq - uint16_t */
/* #define ee_analogoutmode  0x13               /\* analog out selction *\/ */
#define ee_cal_tab_len    0x14                 /* calib table len - uint8_t */

/* #define ee_ 0  /\*  *\/ */
#define ee_lastunused  0x15

#define ee_cal_tab          0x100           /* calibration table */
#define ee_cal_tab_f_offset 0x00
#define ee_cal_tab_b_offset 0x02
#define ee_cal_tab_c_offset 0x04
#define ee_cal_tab_item_len 0x08

uint16_t ext_eeprom_wb(uint16_t addr, uint8_t v),
  ext_eeprom_rb(uint16_t addr, uint8_t* v);

void ee_save(void *var, uint8_t len, uint16_t ee_base_addr),
  ee_load(void *var, uint8_t len, uint16_t ee_base_addr),
    eeprom_default_check_and_fill(uint8_t force);

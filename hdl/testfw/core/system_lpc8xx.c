#include <stdint.h>
#include "LPC82x.h"


/*--------------------------------------------------------------------------
  Check the register settings
 *--------------------------------------------------------------------------*/
#define CHECK_RANGE(val, min, max)                ((val < min) || (val > max))
#define CHECK_RSVD(val, mask)                     (val & mask)

/* Clock Configuration -----------------------------------------------------*/
#if (CHECK_RSVD((SYSOSCCTRL_Val),  ~0x00000003))
   #error "SYSOSCCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((WDTOSCCTRL_Val),  ~0x000001FF))
   #error "WDTOSCCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((SYSPLLCLKSEL_Val), 0, 3))
   #error "SYSPLLCLKSEL: Value out of range!"
#endif

#if (CHECK_RSVD((SYSPLLCTRL_Val),  ~0x000001FF))
   #error "SYSPLLCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((MAINCLKSEL_Val),  ~0x00000003))
   #error "MAINCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((SYSAHBCLKDIV_Val), 0, 255))
   #error "SYSAHBCLKDIV: Value out of range!"
#endif


/*--------------------------------------------------------------------------
  DEFINES
 *--------------------------------------------------------------------------*/
    
/*--------------------------------------------------------------------------
  Define clocks
 *---------------------------------------------------------------------------*/

#define __FREQSEL   ((WDTOSCCTRL_Val >> 5) & 0x0F)
#define __DIVSEL   (((WDTOSCCTRL_Val & 0x1F) << 1) + 2)

#if (CLOCK_SETUP)                         /* Clock Setup              */
  #if  (__FREQSEL ==  0)
    #define __WDT_OSC_CLK        ( 0)                  /* undefined */
  #elif (__FREQSEL ==  1)
    #define __WDT_OSC_CLK        ( 500000 / __DIVSEL)
  #elif (__FREQSEL ==  2)
    #define __WDT_OSC_CLK        ( 800000 / __DIVSEL)
  #elif (__FREQSEL ==  3)
    #define __WDT_OSC_CLK        (1100000 / __DIVSEL)
  #elif (__FREQSEL ==  4)
    #define __WDT_OSC_CLK        (1400000 / __DIVSEL)
  #elif (__FREQSEL ==  5)
    #define __WDT_OSC_CLK        (1600000 / __DIVSEL)
  #elif (__FREQSEL ==  6)
    #define __WDT_OSC_CLK        (1800000 / __DIVSEL)
  #elif (__FREQSEL ==  7)
    #define __WDT_OSC_CLK        (2000000 / __DIVSEL)
  #elif (__FREQSEL ==  8)
    #define __WDT_OSC_CLK        (2200000 / __DIVSEL)
  #elif (__FREQSEL ==  9)
    #define __WDT_OSC_CLK        (2400000 / __DIVSEL)
  #elif (__FREQSEL == 10)
    #define __WDT_OSC_CLK        (2600000 / __DIVSEL)
  #elif (__FREQSEL == 11)
    #define __WDT_OSC_CLK        (2700000 / __DIVSEL)
  #elif (__FREQSEL == 12)
    #define __WDT_OSC_CLK        (2900000 / __DIVSEL)
  #elif (__FREQSEL == 13)
    #define __WDT_OSC_CLK        (3100000 / __DIVSEL)
  #elif (__FREQSEL == 14)
    #define __WDT_OSC_CLK        (3200000 / __DIVSEL)
  #else
    #define __WDT_OSC_CLK        (3400000 / __DIVSEL)
  #endif

  /* sys_pllclkin calculation */
  #if   ((SYSPLLCLKSEL_Val & 0x03) == 0)
    #define __SYS_PLLCLKIN           (__IRC_OSC_CLK)
  #elif ((SYSPLLCLKSEL_Val & 0x03) == 1)
    #define __SYS_PLLCLKIN           (__SYS_OSC_CLK)
  #elif ((SYSPLLCLKSEL_Val & 0x03) == 3)
    #define __SYS_PLLCLKIN           (__CLKIN_CLK)
  #else
    #define __SYS_PLLCLKIN           (0)
  #endif

  #define  __SYS_PLLCLKOUT   (__SYS_PLLCLKIN * ((SYSPLLCTRL_Val & 0x01F) + 1))

  /* main clock calculation */
  #if   ((MAINCLKSEL_Val & 0x03) == 0)
    #define __MAIN_CLOCK             (__IRC_OSC_CLK)
  #elif ((MAINCLKSEL_Val & 0x03) == 1)
    #define __MAIN_CLOCK             (__SYS_PLLCLKIN)
  #elif ((MAINCLKSEL_Val & 0x03) == 2)
    #if (__FREQSEL ==  0)
      #error "MAINCLKSEL: WDT Oscillator selected but FREQSEL is undefined!"
    #else
      #define __MAIN_CLOCK           (__WDT_OSC_CLK)
    #endif
  #elif ((MAINCLKSEL_Val & 0x03) == 3)
    #define __MAIN_CLOCK             (__SYS_PLLCLKOUT)
  #else
    #define __MAIN_CLOCK             (0)
  #endif

  #define __SYSTEM_CLOCK             (__MAIN_CLOCK / SYSAHBCLKDIV_Val)         

#else
  #define __SYSTEM_CLOCK             (__IRC_OSC_CLK)
#endif  // CLOCK_SETUP 


/*------------------------------------------------------------------------
  Clock Variable definitions
 *------------------------------------------------------------------------*/
uint32_t main_clk, sys_clk;

/* initialize system using IRC clock source and PLL */
/* refer to "fig 7. System PLL Block Diagram" 
on pag. 56 of 485 for rev 1 18/09/2014 of UM10800 */
#define IRC_CLK 12000000UL
#define MSEL 5
#define PSEL 4                     /* clkin=12MHz (IRC) ---> FCLKOUT=30MHz */
#define AHBDIV 2
__attribute__ ((section(".after_vectors")))void system_init_IRC(void){

  /* power IOCON and SWM */
  LPC_SYSCON->SYSAHBCLKCTRL|=((0x1<<7) | (0x1<<18));

  /* = clock setup = */
  LPC_SYSCON->SYSPLLCLKSEL=0x00000000;         /* use IRC as pll clk input */
  LPC_SYSCON->SYSPLLCLKUEN=0x00;        /* update clk src (needs two step) */
  LPC_SYSCON->SYSPLLCLKUEN=0x01;        /* update clk src (needs two step) */
  while(!(LPC_SYSCON->SYSPLLCLKUEN & 0x01));         /* wait until updated */  

  /* FCCO= MSEL*clkin ; 156MHz<=FCCO<=320MHz */
  /* FCLKOUT=FCCO/(2*P)=MSEL*clkin/(2*P) */
  LPC_SYSCON->SYSPLLCTRL=((PSEL/2)<<5)+((MSEL-1)<<0);
  LPC_SYSCON->PDRUNCFG&=~(0x1<<7);                         /* power-up pll */
  while(!(LPC_SYSCON->SYSPLLSTAT&0x01));       /* wait until pll is locked */  
  
  LPC_SYSCON->SYSAHBCLKDIV=AHBDIV;                  /* set AHB bus divider */

  for(volatile uint32_t i=0; i<200; i++) __NOP();             /* wait more */

  LPC_SYSCON->MAINCLKSEL=0x00000003;/* now pll out can be used as main clk */
  LPC_SYSCON->MAINCLKUEN=0x00;     /* update main clk src (needs two step) */
  LPC_SYSCON->MAINCLKUEN=0x01;     /* update main clk src (needs two step) */
  while(!(LPC_SYSCON->MAINCLKUEN&0x01));             /* wait until updated */

  main_clk=IRC_CLK*MSEL;
  sys_clk=main_clk/AHBDIV;
  /* # clock setup end # */
  
}

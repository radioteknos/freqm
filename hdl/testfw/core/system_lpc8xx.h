#ifndef __SYSTEM_LPC8xx_H
#define __SYSTEM_LPC8xx_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialize the system
 *
 * @param  none
 * @return none
 *
 * @brief  Setup the microcontroller system.
 *         Initialize the System and update the SystemCoreClock variable.
 */

extern void system_init_IRC(void);


#ifdef __cplusplus
}
#endif

#endif /* __SYSTEM_LPC8xx_H */

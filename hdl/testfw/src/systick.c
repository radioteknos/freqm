/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdint.h>
#include "LPC82x.h"
#include "main.h"
#include "systick.h"

volatile uint32_t delay_ms_ticks=0, systick_to0_cnt=0;
extern volatile uint32_t stat;

/* TODO: rewrite together cmsis elimination from core/ dir */
uint32_t systick_init(uint32_t ticks){
  if((ticks - 1)>SysTick_LOAD_RELOAD_Msk)
    return 1;      /* reload value impossible */

  /* set reload register */
  SysTick->LOAD=ticks-1;

  /* set priority for systick int */
  NVIC_SetPriority(SysTick_IRQn, (1<<__NVIC_PRIO_BITS)-1);  

  /* load the systick counter val */
  SysTick->VAL=0;
  
  /* Enable SysTick IRQ and SysTick Timer */
  SysTick->CTRL=SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk |
    SysTick_CTRL_ENABLE_Msk;
  
  return 0;
}

void systick_isr(void){
  static uint16_t presc_tasks_cnt=PRESCTASKCNTVAL;;
  
  /* = base period tasks = */  
  /* = systick_timeout0 = */
  if(systick_to0_cnt!=0)
    systick_to0_cnt--;
  /* # systick_timeout0 end # */

  
  /* = blocking delay routine counter = */
  delay_ms_ticks++;
  /* # blocking delay routine counter end # */
  /* base period tasks end # */

  
  /* = prescaled period tasks = */
  /* 10ms tasks */
  if(presc_tasks_cnt==0){

    presc_tasks_cnt=PRESCTASKCNTVAL;
  }
  else
    presc_tasks_cnt--;  
  /* prescaled period tasks end # */
  
}

    
  
void delay_ms(uint32_t ms){
  uint32_t now=delay_ms_ticks;
  while((delay_ms_ticks-now)<ms);
}

/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include "LPC82x.h"
#include <stdint.h>
#include <stdlib.h>
#include "main.h"
#include "uart.h"
#include "systick.h"
#include "spi.h"


void spi0_init(void){

  LPC_SYSCON->SYSAHBCLKCTRL|=(1<<11);               /* enable clock to SPI0 */
  LPC_SYSCON->PRESETCTRL&=~(1<<0);
  LPC_SYSCON->PRESETCTRL|=(1<<0);                            /* reset SPIO0 */

  LPC_SPI0->DIV=49;                                 /* 600kHz clock for SPI */

  LPC_SPI0->CFG=CFG_MASTER | CFG_ENABLE;           /* enable SPI0 as master */
}

void spi0_isr(void){

  LPC_SPI0->INTENCLR=0x0000003f;                     /* clear all intterupt */
  
}


uint32_t spi8_send_get(uint32_t v){
  uint32_t rv;

  ss_low();
  for(rv=0; rv<100; rv++) /* TODO: remove/reduce (?) */
    asm("nop");


  while((LPC_SPI0->STAT & STAT_TXRDY)==0);
  LPC_SPI0->TXDATCTL=TXDATCTL_FSIZE(7) | TXDATCTL_SSELN(0xe) |
    TXDATCTL_EOT | (v&0xff);
  while((LPC_SPI0->STAT & STAT_RXRDY)==0);
  rv=LPC_SPI0->RXDAT;
  
  ss_high();

  return(rv&0xff);
  
}

/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include "LPC82x.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "uart.h"
#include "i2c.h"
#include "systick.h"
#include "eeprom.h"

extern volatile uint32_t stat;
extern uint16_t fmeas;
extern volatile uint16_t mrate;
extern int8_t nosigthr;             /* below nosigthr no readings are shown */
extern uint8_t mode;

uint16_t ext_eeprom_wb(uint16_t addr, uint8_t v){
  uint16_t rv=0;
#ifdef M24LC08
  uint8_t block;
#endif
  
#ifdef M24LC256
  if(i2c0_send_start_addr(EEADDR))
    rv|=0x0001;

  if(i2c0_send_data(addr>>8))
    rv|=0x0002;
#endif

#ifdef M24LC08
  block=(addr>>8)&0x3;
  if(i2c0_send_start_addr(EEADDR|block))
    rv|=0x0001;
#endif
  
  if(i2c0_send_data(addr&0xff))
    rv|=0x0004;

  if(i2c0_send_data(v))
    rv|=0x0008;

  i2c0_send_stop();

  while(!(LPC_I2C0->STAT & MSTPENDING));

  if((LPC_I2C0->STAT & MSTSTATE) != MSTSTATE_IDLE)
    rv|=0x0010;
    
  return rv;
}

uint16_t ext_eeprom_rb(uint16_t addr, uint8_t* v){
  uint16_t rv=0;
#ifdef M24LC08
  uint8_t block;
#endif
  
#ifdef M24LC256
  if(i2c0_send_start_addr(EEADDR))
    rv|=0x0001;
    
  if(i2c0_send_data(addr>>8))
    rv|=0x0002;
#endif
#ifdef M24LC08
  block=(addr>>8)&0x3;
  if(i2c0_send_start_addr(EEADDR|block))
    rv|=0x0001;
#endif
  
  if(i2c0_send_data(addr&0xff))
    rv|=0x0004;

#ifdef M24LC08
  if(i2c0_receive_data(EEADDR|block, v))
    rv|=0x0008;
#endif
#ifdef M24LC256
  if(i2c0_receive_data(EEADDR, v))
    rv|=0x0008;
#endif  
  
  i2c0_send_stop();
  while(!(LPC_I2C0->STAT & MSTPENDING));
  if((LPC_I2C0->STAT & MSTSTATE) != MSTSTATE_IDLE)
    rv|=0x0010;
    
  return rv;
}

void ee_save(void *var, uint8_t len, uint16_t ee_base_addr){
  uint8_t i;

  for(i=0; i<len; i++){
    ext_eeprom_wb(ee_base_addr+i, *((uint8_t *)var+i));
    delay_ms(6);
  }
}

void ee_load(void *var, uint8_t len, uint16_t ee_base_addr){
  uint8_t i;
  
  for(i=0; i<len; i++)
    ext_eeprom_rb(ee_base_addr+i, ((uint8_t *)var+i));
}

void eeprom_default_check_and_fill(uint8_t force){
  uint8_t v;
  
  /* ee_serno */
  ext_eeprom_rb(ee_serno, &v);
  if(v==0xff || force==1){
    ext_eeprom_wb(ee_serno, '0');
    delay_ms(6);
    ext_eeprom_wb(ee_serno+1, '0');
    delay_ms(6);
    ext_eeprom_wb(ee_serno+2, '0');
    delay_ms(6);
  }

}

/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"
#include "uart.h"
#include "math.h"
#include "misc.h"

extern char buff[BUFFLEN+1];
extern volatile uint8_t uart0_rxwp, uart0_rxrp, uart0_msgn;

uint32_t skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

uint32_t skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

/* unsigned int isqrt(unsigned int a){ */
/*   unsigned long square = 1; */
/*   unsigned long delta = 3; */
/*   while(square <= a){ */
/*      square = square + delta; */
/*      delta = delta + 2; */
/*   } */
/*      return (delta/2 - 1); */
/* } */

void fp_ftoa(char *b, float v, uint8_t dpn){
  uint8_t i;
  char *p;
  int32_t ifps;

  i=dpn;
  
  while(i!=0){
    v*=10.;
    i--;
  }
  if(v>0)
    ifps=(int)(v+0.5);
  else
    ifps=(int)(v-0.5);
  
  i=dpn;
  
  itoa(ifps, b, 10);
  
  p=b+strlen(b);
  *(p+1)='\0';
  do{
    *p=*(p-1);
    p--;
    i--;
  } while(i!=0);
  b[strlen(b)-dpn-1]='.';  
}

void get_answer(char *ans){
  char *pb;
  uint8_t i;

  while(uart0_msgn==0);
  uart0_get_next_msg(buff);

  /* flush off spaces and tabs */
  pb=(char*)buff; skipspace(&pb);

  /* remove trailing \r \n */
  for(i=0; i<strlen(pb); i++)
    if(pb[i]=='\n' || pb[i]=='\r'){
      pb[i]='\0';
      break;
    }

  strncpy(ans, pb, 16); ans[15]='\0';
}

float fp_atof(char *b){
  uint8_t i;
  int8_t dp;
  float v;

  dp=-1;
  for(i=0; i<strlen(b); i++){
    if(b[i]=='.')
      dp=strlen(b)-i-1;
    if(dp!=-1)
      b[i]=b[i+1];
  }
  v=(float)atoi(b);  
  for(i=0; i<dp; i++)
    v/=10.;
      
  return v;
}

float dBm_to_mW(float pwrdbm){
  float mw;

  mw=powf(10., pwrdbm/10.);
  /* mw=pow10f(pwrdbm/10.); */
  
  return mw;
}

/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include "LPC82x.h"
#include <stdint.h>
#include <stdlib.h>
#include "main.h"
#include "uart.h"
#include "i2c.h"

extern char buff[BUFFLEN+1];
extern volatile uint32_t systick_to0_cnt;

void i2c0_init(void){

  LPC_IOCON->PIO0_11=0x80;
  LPC_IOCON->PIO0_10=0x80;
  LPC_SYSCON->SYSAHBCLKCTRL|=(1<<5);                /* enable clock to I2C0 */
  LPC_SYSCON->PRESETCTRL&=~(1<<6);
  LPC_SYSCON->PRESETCTRL|=(1<<6);                             /* reset I2C0 */

  LPC_I2C0->CLKDIV=74;                              /* 100kHz clock for I2C */
  LPC_I2C0->MSTTIME=0;                                                  /*  */
  LPC_I2C0->CFG=(1<<MSTEN);                                  /* master mode */
  
}


uint16_t i2c0_send_start_addr(uint8_t devaddr){
  /* 7bit device addr! */

  while(!(LPC_I2C0->STAT & MSTPENDING));   /* wait for i2c0 idle or pending */
  if((LPC_I2C0->STAT & MSTSTATE) == MSTSTATE_IDLE){
    LPC_I2C0->MSTDAT=(devaddr<<1);               /* send addr w/ R/W bit =0 */
    LPC_I2C0->MSTCTL=(1<<MSTSTART);                 /* send start condition */
  }
  else
    return 1;
  
  while(!(LPC_I2C0->STAT & MSTPENDING));   /* wait for i2c0 idle or pending */
  
  return 0;
}

uint16_t i2c0_send_data(uint8_t v){

  
  if((LPC_I2C0->STAT & MSTSTATE) == MSTSTATE_TXRDY){
    LPC_I2C0->MSTDAT=(v);                                      /* send data */
    LPC_I2C0->MSTCTL=(1<<MSTCONTINUE);
  }
  else{
    itoa(LPC_I2C0->STAT, buff, 16); uart0_puts(buff);    
    return 1;
  }
  
  while(!(LPC_I2C0->STAT & MSTPENDING));   /* wait for i2c0 idle or pending */
  
  return 0;
}

uint16_t i2c0_receive_data(uint8_t devaddr, uint8_t* v){

  while(!(LPC_I2C0->STAT & MSTPENDING));   /* wait for i2c0 idle or pending */

  /* TODO: check if in right state */
  LPC_I2C0->MSTDAT=(devaddr<< 1)|0x1;            /* send addr w/ R/W bit =1 */
  LPC_I2C0->MSTCTL=(1<<MSTSTART);                  /* send start condition */
  while(!(LPC_I2C0->STAT & MSTPENDING));   /* wait for i2c0 idle or pending */
    
  
  *v=LPC_I2C0->MSTDAT;
  
  return 0;
}

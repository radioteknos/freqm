#include "LPC82x.h"
#include <stdint.h>
#include "uart.h"

/* === UART0 === */

volatile uint8_t uart0_rxbuff[RXBUFFLEN];
volatile uint32_t uart0_stat=0x00000000;
volatile uint8_t uart0_rxwp=0, uart0_rxrp=0, uart0_msgn=0;

extern uint32_t main_clk, sys_clk;


/* TODO: cleanup */
void uart0_init(uint32_t baudrate){
  uint32_t UARTSysClk;
  
  uart0_stat=(1<<uartecho)|(1L<<txempty);
	
  /* = uart0 clock init = */
  LPC_SYSCON->UARTCLKDIV=1;     /* divided by 1 */
	
  NVIC_DisableIRQ(UART0_IRQn);
  /* Enable UART clock */
  LPC_SYSCON->SYSAHBCLKCTRL|=(1<<14);
  /* Peripheral reset control to UART, a "1" bring it out of reset. */
  LPC_SYSCON->PRESETCTRL&=~(0x1<<3);
  LPC_SYSCON->PRESETCTRL|=(0x1<<3);
  /* # uart0 clock init end # */
	
  UARTSysClk = main_clk/LPC_SYSCON->UARTCLKDIV;
	
  /* TODO: use uart0_init() call args */
  LPC_USART0->CFG = DATA_LENG_8|PARITY_NONE|STOP_BIT_1; /* N81 */
  /* LPC_USART0->CFG = DATA_LENG_7|PARITY_NONE|STOP_BIT_1; */ /* 7N1 */
  /* LPC_USART0->CFG = DATA_LENG_8|PARITY_NONE|STOP_BIT_2; */ /* 8N2 */
  /* LPC_USART0->CFG = DATA_LENG_8|PARITY_EVEN|STOP_BIT_1; */ /* 8E1 */
  /* LPC_USART0->CFG = DATA_LENG_8|PARITY_ODD|STOP_BIT_1; */ /* 8O1 */

  /* Baudrate setup */
  LPC_USART0->BRG=UARTSysClk/16/baudrate-1;
  LPC_SYSCON->UARTFRGDIV=0xff;
  LPC_SYSCON->UARTFRGMULT=(((UARTSysClk / 16)*(LPC_SYSCON->UARTFRGDIV + 1))/
			   (baudrate * (LPC_USART0->BRG + 1)))-
    (LPC_SYSCON->UARTFRGDIV + 1);
  		
  /* clear stat */
  LPC_USART0->STAT = CTS_DELTA | DELTA_RXBRK;

  /* enable uart0 irq */
  NVIC_EnableIRQ(UART0_IRQn);
  
  /* select uart0 irq */
  LPC_USART0->INTENSET = RXRDY | TXRDY | DELTA_RXBRK;
   
  /* mask uart error source */
  LPC_USART0->INTENSET = (FRM_ERR|OVRN_ERR|PAR_ERR|RXNOISE);
  
  /* enable uart0*/
  LPC_USART0->CFG |= UART_EN; 
}

void uart0_isr(void){
  uint8_t rxb;

  /* = handle rx incoming data (RXRDY) = */
  if(LPC_USART0->STAT&RXRDY){
    rxb=(uint8_t)LPC_USART0->RXDATA;
    
    if(rxb==0x08 || rxb==0x7f){                 /* simple backspace */
      while(!(LPC_USART0->STAT&TXRDY));
      LPC_USART0->TXDATA='\b';
      while(!(LPC_USART0->STAT&TXRDY));
      LPC_USART0->TXDATA=' ';
      while(!(LPC_USART0->STAT&TXRDY));
      LPC_USART0->TXDATA='\b';
      while(!(LPC_USART0->STAT&TXRDY));
      LPC_USART0->INTENSET=TXRDY;
      if(uart0_rxwp!=uart0_rxrp){
	if(uart0_rxwp>0)
	  uart0_rxwp--;
	else
	  uart0_rxwp=RXBUFFLEN-1;
      }
    }
    else{
      uart0_rxbuff[uart0_rxwp]=rxb;
      
      if(uart0_stat&(1<<uartecho)){
	while(!(LPC_USART0->STAT&TXRDY));
	LPC_USART0->TXDATA=rxb;
      }
      
      if(rxb=='\n' || rxb=='\r'){  /* if rx'ed byte is a line terminator */
	uart0_msgn++;      
      }

      uart0_rxwp++;
      if(uart0_rxwp>RXBUFFLEN-1)
	uart0_rxwp=0;
      if(uart0_rxwp==uart0_rxrp){
	while(!(LPC_USART0->STAT&TXRDY));
	LPC_USART0->TXDATA='\a';
	while(!(LPC_USART0->STAT&TXRDY));
	LPC_USART0->INTENSET=TXRDY;
	uart0_rxwp=0; /* TODO: better (save the queue!) */
	uart0_rxrp=0;
	uart0_rxbuff[uart0_rxwp]='\0';
	uart0_msgn=0;	
      }
    }
  } /* # handle rx incoming data end # */
  
    /* = tx is ready to accept new data = */
  if(LPC_USART0->STAT&TXRDY){
    uart0_stat|=(1L<<txempty);
    LPC_USART0->INTENCLR=TXRDY;
  } /* # tx is ready to accept new data end # */
  

    /* = uart0 error handling = */
  if(LPC_USART0->STAT&UART_ERROR_MASK){
    uart0_stat|=(1L<<rxerror);
    
    if((LPC_USART0->STAT&UART_ERROR_MASK)==DELTA_RXBRK){
      uart0_stat|=(1L<<rxbreak);
      LPC_USART0->STAT=DELTA_RXBRK;
    }
    if((LPC_USART0->STAT&UART_ERROR_MASK)==FRM_ERR){
      uart0_stat|=(1L<<rxfrmerr);
      LPC_USART0->STAT=FRM_ERR;
    }
    if((LPC_USART0->STAT&UART_ERROR_MASK)==OVRN_ERR){
      uart0_stat|=(1L<<rxovrerr);
      LPC_USART0->STAT=OVRN_ERR;
    }
    if((LPC_USART0->STAT&UART_ERROR_MASK)==RXNOISE){
      uart0_stat|=(1L<<rxnoiseerr);
      LPC_USART0->STAT=RXNOISE;
    }
    if((LPC_USART0->STAT&UART_ERROR_MASK)==PAR_ERR){
      uart0_stat|=(1L<<rxparerr);
      LPC_USART0->STAT=PAR_ERR;
    }
    LPC_USART0->INTENCLR=TXRDY;
  }  /* # uart0 error handling end # */
  
}

int uart0_get_next_msg(char *b){
  int i=0;
  char t;

  if(uart0_msgn==0)
    return 0;
  else{
    while(uart0_rxrp!=uart0_rxwp){
      t=uart0_rxbuff[uart0_rxrp];
      b[i]=t;
      uart0_rxrp++;
      if(uart0_rxrp>RXBUFFLEN-1)
	uart0_rxrp=0;
      i++;
      if(t=='\r' || t=='\n'){
	b[i]='\0';
	break;
      }
    }
    if(b[i]!='\0')
      return -1;
    uart0_msgn--;
    return 1;
  }
}

/* high level uart send functions */
void uart0_putc(char c){
  /* while(!(uart0txempty&0x01)); */
  while((uart0_stat&(1L<<txempty))==0);
    LPC_USART0->TXDATA=c;    
    uart0_stat&=~(1L<<txempty);
    LPC_USART0->INTENSET=TXRDY;
}

void uart0_puts(char* b){
  while(*b!='\0'){
    uart0_putc(*b);
    b++;
  }
}


/* === UART1 === */
/* TODO */

/* === UART2 === */
/* TODO */

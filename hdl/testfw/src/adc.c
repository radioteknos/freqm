/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "LPC82x.h"
#include "systick.h"
#include "misc.h"
#include "spi.h"
#include "main.h"
#include "adc.h"

extern volatile uint32_t stat;

extern char buff[BUFFLEN+1];

void adc_init(void){
  uint32_t tmp;
  
  LPC_SYSCON->PDRUNCFG&=~(1<<4);                         /* enable adc pwr */
  LPC_SYSCON->SYSAHBCLKCTRL|=(1<<24);              /* enable clock to ADC */
  LPC_ADC->SEQA_CTRL&=~(1<<31);          /* disable adc seq (SEQA_EN bit) */
  LPC_ADC->CTRL=120;                             /* ADC use 25 clocks so: 
					  main_clk (30MHz) / 120 = 10kS/s */
  
  /* auto calib */
  tmp=LPC_ADC->CTRL;
  LPC_ADC->CTRL=60 | (1<<30);
  while(LPC_ADC->CTRL&(1<<30));
  LPC_ADC->CTRL=tmp;
}


uint16_t adc_get(uint8_t ch){

  LPC_ADC->SEQA_CTRL&=0xfffff000;
  LPC_ADC->SEQA_CTRL|=(1<<ch);                               /* select ch */

  LPC_ADC->SEQA_CTRL|=(1<<31);                                /* seq A en */

  LPC_ADC->SEQA_CTRL|=(1<<26);                     /* start of conversion */
  

  while((LPC_ADC->SEQA_GDAT&(1<<31))==0);       /* wait end of conversion */
                                                     /* TODO: add timeout */
  
  return((LPC_ADC->SEQA_GDAT>>4)&0xfff);
}

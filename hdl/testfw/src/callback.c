/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "LPC82x.h"
#include "uart.h"
#include "main.h"
#include "misc.h"
#include "spi.h"
#include "i2c.h"
#include "eeprom.h"
#include "systick.h"
#include "adc.h"
#include "callback.h"

extern volatile uint32_t stat;
extern uint32_t main_clk, sys_clk;
extern volatile char uart0_rxbuff[RXBUFFLEN];
extern volatile uint32_t uart0_stat;
extern volatile uint8_t uart0_rxwp, uart0_rxrp, uart0_msgn;

extern char buff[BUFFLEN+1];

extern uint16_t B;
extern float C;
extern uint16_t fmeas;                       /* measurement frequency [MHz] */

uint32_t cmd_getstat(char* params __attribute__((unused))){

  uart0_puts("\r\n\nsystem core clock ");
  itoa(sys_clk, buff, 10); uart0_puts(buff);  

  uart0_puts("\r\nuart0_stat ");
  itoa(uart0_stat, buff, 16); uart0_puts(buff);

  uart0_puts("\r\nstat ");
  itoa(stat, buff, 16); uart0_puts(buff);

  uart0_puts("\r\nLPC_USART0->STAT ");
  itoa(LPC_USART0->STAT, buff, 16); uart0_puts(buff);

  uart0_puts("\r\nLPC_SPI0->STAT ");
  itoa(LPC_SPI0->STAT, buff, 16); uart0_puts(buff);

  uart0_send_crlf();
  
  return 0;
}

uint32_t cmd_eeprom_wb(char* params){
  uint16_t addr, rv;
  uint8_t data;
  char *pb;

  pb=params; skipspace(&pb);
  addr=strtol(pb, NULL, 16);
  skipnotspace(&pb); skipspace(&pb);
  data=strtol(pb, NULL, 16);

  rv=ext_eeprom_wb(addr, data);
  itoa(rv, buff, 16); uart0_puts(buff); uart0_send_crlf();
  
  return 0;
}

uint32_t cmd_eeprom_rb(char* params){
  uint16_t addr, rv;
  uint8_t data;
  char *pb;

  pb=params; skipspace(&pb);
  addr=strtol(pb, NULL, 16);

  rv=ext_eeprom_rb(addr, &data);
  
  uart0_send_crlf();
  itoa(data, buff, 16); uart0_puts(buff);
  uart0_puts("\t'"); uart0_putc(data); uart0_puts("\'\t(");
  itoa(rv, buff, 16); uart0_puts(buff); uart0_puts(")\n");
  
  return 0;
}

uint32_t cmd_spi_send_get(char* params){
  uint32_t v, rv;
  char *pb;

  pb=params; skipspace(&pb);
  v=strtol(pb, NULL, 16);

  rv=spi8_send_get(v);
  
  uart0_send_crlf();
  itoa(rv, buff, 16); uart0_puts(buff);
  uart0_send_crlf();
  
  return 0;
}


/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "LPC82x.h"
#include "systick.h"
#include "uart.h"
#include "callback.h"
#include "misc.h"
#include "spi.h"
#include "i2c.h"
#include "eeprom.h"
#include "adc.h"
#include "pwm.h"
#include "main.h"

volatile uint32_t stat=0x000000;
extern uint32_t main_clk, sys_clk;
extern volatile char uart0_rxbuff[RXBUFFLEN];
extern volatile uint32_t uart0_stat;
extern volatile uint8_t uart0_rxwp, uart0_rxrp, uart0_msgn;

char buff[BUFFLEN+1];

uint16_t B;
float C;
uint16_t fmeas;                              /* measurement frequency [MHz] */

struct CMDS{
  char name[9];
  uint32_t (*funct)(char * params);
};

const struct CMDS cmds[] ={
  /* cmdlen=7 */
  {"getstat", cmd_getstat},

  /* cmdlen=6 */

  /* cmdlen=5 */

  /* cmdlen=4 */

  /* cmdlen=3 */
  {"eew", cmd_eeprom_wb},
  {"eer", cmd_eeprom_rb},
  {"spi", cmd_spi_send_get},
  
  /* cmdlen=2 */

  /* cmdlen=1 */
};


int main(void){
  char *pb, params[16], cmd[16], msgbuff[32];
  uint32_t i, rv;
  
  /* = system init = */
  SysTick_Config(sys_clk/1000);                          /* number of ticks */
  system_init_IRC();                   /* system config and init w/ IRC osc */
  LPC_SYSCON->SYSAHBCLKCTRL|=(1<<18);              /* enable clock to IOCON */
  LPC_SYSCON->BODCTRL=0x00000013;   /* BOD reset enable at lev3: 2.63-2.78V */
  /* # system init end # */

  /* = variable (no eeprom load) init = */
  stat=0x0;
  /* # variable (no eeprom load) init end # */
  
  /* = SWM setup = */
  LPC_SWM->PINASSIGN0&=0xffffff00;
  LPC_SWM->PINASSIGN0|=0x0004;                                  /* uart txd */
  LPC_SWM->PINASSIGN0&=0xffff00ff;
  LPC_SWM->PINASSIGN0|=0x0000;                                  /* uart rxd */
  LPC_SWM->PINASSIGN4&=0xffff00ff;
  LPC_SWM->PINASSIGN4|=0x00000d00;                             /* spi0 miso */
  LPC_SWM->PINASSIGN3&=0x00ffffff;
  LPC_SWM->PINASSIGN3|=0x02000000;                              /* spi0 sck */
  LPC_SWM->PINASSIGN4&=0xffffff00;
  LPC_SWM->PINASSIGN4|=0x000000011;                             /* spi0 mosi */
   
  LPC_SWM->PINENABLE0|=(1<<4);          /* disconnect SWCLK, PIO0_3 as GPIO */
  LPC_SWM->PINENABLE0|=(1<<5);          /* disconnect SWDIO, PIO0_2 as GPIO */
  LPC_SWM->PINENABLE0&=~(1<<11);             /* connect I2C0_SDA to PIO0_11 */
  LPC_SWM->PINENABLE0&=~(1<<12);             /* connect I2C0_SCL to PIO0_10 */

    
  
  LPC_SYSCON->SYSAHBCLKCTRL&=~(1<<7);               /* disable clock to SWM */
  /* # SWM setup end # */

  /* = GPIO setup = */
  LPC_SYSCON->SYSAHBCLKCTRL |= (1<<6);              /* enable clock to GPIO */

  LPC_SYSCON->PRESETCTRL &= ~(0x1<<10);                       /* GPIO reset */
  LPC_SYSCON->PRESETCTRL |= (0x1<<10);

  LPC_GPIO_PORT->DIR0|=(1<<SCK)|(1<<SS);                       /* spi0 pins */


  LPC_IOCON->PIO0_13=0x00000080;             /* PIO0_13 as MISO: in (pin 3) */
  LPC_IOCON->PIO0_2=0x00000080;           /* PIO0_2 as SPI0SCK: out (pin 8) */
  LPC_IOCON->PIO0_17=0x00000080;           /* PIO0_17 as SELAB: out (pin 2) */
  LPC_IOCON->PIO0_1=0x00000080;            /* PIO0_1 as ADCSS: out (pin 12) */


  LPC_GPIO_PORT->DIR0|=(1<<MOSI);                               /* MOSI out */
  LPC_GPIO_PORT->DIR0|=(1<<SCK);                                 /* SCK out */
  LPC_GPIO_PORT->DIR0|=(1<<SS);                                   /* SS out */
  LPC_GPIO_PORT->DIR0&=~(1<<MISO);                             /* SYNTMO in */
  
  
  /* # GPIO setup end # */
  
  /* = internal peripherial init = */
  uart0_init(BAUDRATE);
  spi0_init();
  i2c0_init();
  /* # internal peripherial init end # */

  /* check for valid external eeprom data */
  eeprom_default_check_and_fill(0);
  
  /* = external peripherial init = */
  /* # external peripherial init end # */

  /* = variable (eeprom load) init = */
  /* # variable (eeprom load) init end # */
  
  /* = uart hello message = */
  uart0_puts("\r\n\n\thdlspitest");
  uart0_puts(" hw v");
  uart0_puts(__HW_VERSION);
  uart0_puts(" fw v");
  uart0_puts(__FW_VERSION);
  uart0_puts("\r\n\tRadioteknos  www.radioteknos.it\r\n");
  /* # uart hello message end # */

  /* ready to go! */
  uart0_send_prompt();
  
  /* = main loop = */
  while(1){ 

    if(uart0_stat&(1L<<rxerror)){
      rv=LPC_USART0->STAT;
      uart0_puts("\r\n[rxerr stat: ");
      itoa(rv, buff, 16); uart0_puts(buff);      
      uart0_puts("] uart0_stat ");
      itoa(uart0_stat, buff, 16); uart0_puts(buff);
      uart0_send_prompt();      
      uart0_stat&=~((1L<<rxerror) | (1L<<rxbreak) | (1L<<rxfrmerr) | 
		    (1L<<rxovrerr) | (1L<<rxnoiseerr) | (1L<<rxparerr));
    }

    /* = serial data decoding = */
    if(uart0_msgn>0){
      uart0_get_next_msg(msgbuff);

      /* flush off spaces and tabs */
      pb=(char*)msgbuff; skipspace(&pb);
      
      /* remove trailing \r \n */
      for(i=0; i<strlen(pb); i++)
	if(pb[i]=='\n' || pb[i]=='\r'){
	  pb[i]='\0';
	  break;
	}
      
      if(strlen(pb)!=0){
	
	/* = scan cmds = */
	strncpy(cmd, pb, sizeof(cmd)/sizeof(char));
	for(i=0; i<strlen(cmd); i++)
	  if(cmd[i]==' '){
	    cmd[i]='\0';
	    break;
	  }
	
	for(i=0; i<sizeof(cmds)/sizeof(struct CMDS); i++){
	  if(strcmp(cmd, cmds[i].name)==0){
	    if(strlen(pb)==strlen(cmds[i].name))
	      params[0]='\0';
	    else
	      strncpy(params, pb+strlen(cmds[i].name)+1, 16);
	    
	    rv=cmds[i].funct(params);
	    
	    if(rv!=0){
	      uart0_puts("\r\n! error !\r\n");	  
	    }
	    break;
	  }
	}
	if(i==sizeof(cmds)/sizeof(struct CMDS)){
	  uart0_send_crlf();
	  uart0_puts(pb);
	  uart0_send_error();
	}
      } /* # scan cmds end # */
      
      uart0_send_prompt(); 
      /* if strlen(pb)==0 the line is empty or just a <CR><LF> has been
	 received, so only a new prompt is send to show that uC is alive */
            
    }  /* # serial data decoding end # */



    
    
  } /* # main loop end # */
  
  
}

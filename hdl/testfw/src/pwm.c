/*
 *   pwrm1-rfbctrl
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "LPC82x.h"
#include "systick.h"
#include "uart.h"
#include "misc.h"
#include "pwm.h"
#include "main.h"

extern volatile uint32_t stat;

extern char buff[BUFFLEN+1];
extern uint32_t main_clk, sys_clk;
volatile uint32_t pwmtics;

void pwm_init(void){

  LPC_SYSCON->SYSAHBCLKCTRL|=(1<<8);                /* enable clock to SCT */
  LPC_SYSCON->PRESETCTRL&=~(1<<8);
  LPC_SYSCON->PRESETCTRL|=(1<<8);                             /* reset SCT */

  LPC_SCT->CONFIG=0x00000001;             /* one 32bit counter; system clk */
  
  LPC_SCT->OUTPUT=(7 << 0);         /* initial out state high (why all???) */ 

  pwmtics=sys_clk/PWMFREQ;
  
  LPC_SCT->REGMODE=0;           /* all (why all???) SCT regs work as match */


  LPC_SCT->MATCH[0]=pwmtics;    /* event 0 reset counter , i.e. pwm period */
  LPC_SCT->MATCHREL[0]=pwmtics;
  LPC_SCT->EVENT[0].CTRL=0x00001000;
  LPC_SCT->EVENT[0].STATE=0xFFFFFFFF;
  LPC_SCT->LIMIT=(1<<0);


  LPC_SCT->OUT[0].CLR = (1 << 0);                     /* event 1 clear out */
  /* LPC_SCT->OUT[1].CLR = (1 << 0);                                   /\* ??? *\/ */
  /* LPC_SCT->OUT[2].CLR = (1 << 0);                                   /\* ??? *\/ */

  LPC_SCT->EVENT[1].CTRL=(1<<0)|(1<<12);     /* event 1 on match 1 set out */
  LPC_SCT->EVENT[1].STATE = 1;
  LPC_SCT->OUT[0].SET = (1 << 1);

  LPC_SCT->STATE = 0;                                    /* state not used */

  LPC_SCT->CTRL &= ~(1 << 2);                             /* start counter */
}

void pwm_set(uint16_t ppt){
  uint32_t v;

  if(ppt>1000)
    v=1000;
  else if(ppt==0)
    v=pwmtics+1;
  else
    v=pwmtics-(pwmtics*ppt)/1000;

  LPC_SCT->MATCHREL[1]=v;
}

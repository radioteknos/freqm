/*
 *   freqm - lab frequency meter
 * 
 *      (c) by Lapo Pieri, Giluio Fieramosca 2020-2021
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

module freqm(
	     /* iCEstick clk (12MHz) */
	     input  clkin,

	     /* 10MHz ref_in */
	     input  ref_in,

	     /* iCEstick leds */
	     output LED_GREEN, 
	     output LED0, output LED1, output LED2, output LED3,

	     /* time base out (debug, remove) */
	     output tb_out,

	     /* spi (slave) */
	     input spi_mosi,
	     output spi_miso,
	     input spi_sck,
	     input spi_ss	     
	     );   

   
   SB_PLL40_CORE #(
		   .FEEDBACK_PATH("SIMPLE"),
		                        /* $ icepll -i 12 -o 30 */
		                                /* clkin=12MHz */
		   .DIVR(0),		/* DIVR =  0  (div 1) */
		   .DIVF(79),    	/* DIVF = 79 (x80) */
		   .DIVQ(5),		/* DIVQ =  5 (div 6) */
		                                /* main_clk=30MHz */
		   .FILTER_RANGE(3'b001)	/* FILTER_RANGE = 1 */
		   ) uut (
			  .LOCK(LED_GREEN),
			  .RESETB(1'b1),
			  .BYPASS(1'b0),
			  .REFERENCECLK(clkin),
			  .PLLOUTCORE(main_clk)
			  );

   
   wire main_clk;
   wire ref_in, t1;
   wire tb1e5, tb1e3;
   wire tb500, tb200, tb100, tb50, tb20, tb10, tb5, tb2, tb1, 
	tb2s, tb5s, tb10s;
   reg tb;
   reg [3:0] cfg;
   reg [7:0] spitxdata, spirxdata;
   reg 	     dbg;
   
   

   /* mother board don't hav dipsw and spi comm is not ready */
   initial
     begin
	cfg=4'b0001; /* 500Hz tb_out */
     end

   /* time base (tb) dividers */      
   div100 d100_1(ref_in, tb1e5);
   div100 d100_2(tb1e5, tb1e3);

   div2 d2_1(tb1e3, tb500);
   div5 d5_1(tb1e3, tb200);
   div2 d2_2(tb200, tb100);

   div2 d2_3(tb100, tb50);
   div5 d5_2(tb100, tb20);
   div2 d2_4(tb20, tb10);
   
   div2 d2_5(tb10, tb5);
   div5 d5_3(tb10, tb2);
   div2 d2_6(tb2, tb1);
   
   div2 d2_7(tb1, tb2s);
   div5 d5_4(tb1, tb5s);
   div2 d2_8(tb5s, tb10s);

   /* tb selector */
always @ *
   case(cfg)     
     4'b0000: tb=tb1e3;
     4'b0001: tb=tb500;     
     4'b0000: tb=tb1e3;
     4'b0001: tb=tb500;     
     4'b0010: tb=tb200;
     4'b0011: tb=tb100;
     4'b0100: tb=tb50;
     4'b0101: tb=tb20;
     4'b0110: tb=tb10;
     4'b0111: tb=tb5;
     4'b1000: tb=tb2;
     4'b1001: tb=tb1;
     4'b1010: tb=tb2s;
     4'b1011: tb=tb5s;
     4'b1100: tb=tb10s;
     default: tb=ref_in;     
   endcase


   /* = spi = */
   reg rxavail;

   spi_slave spi0(spi_sck, spi_mosi, spi_ss, spi_miso, spitxdata, spirxdata,
		  main_clk, rxavail);
   

   always @(posedge rxavail)
     begin	
	spitxdata<=spirxdata+8'h01;
	if(spirxdata==8'h55)
	  dbg=1'b1;
	else
	  dbg=1'b0;
     end
   
   /* # spi end # */
   
   /* TODO: remove (debug) */
   assign tb_out=tb;   
   
   /* TODO: remove (debug) */
   assign LED0=tb_out;
   assign LED1=main_clk;
   assign LED2=tb2;
   assign LED3=dbg;
   
endmodule /* # freqm end # */

/* ---------------------------------------------------------------- */

module spi_slave(input sck, input mosi, input ss, output miso,
		 input [7:0] txdata, output [7:0] rxdata, 
		 input clk, output rxdataavail);
   
   reg [7:0]           rxdata = 8'b0;

   reg [8:0] 	       shr_tx = 8'b0;
   reg [7:0] 	       shr_rx = 8'b0;
   reg [1:0] 	       sck_r = 2'b0;
   reg [1:0]           ss_r = 2'b0;

   reg 		       rxdataavail;
   
   /* mode 0: CPOL 0, CPHS 0 */
   wire shift;
   wire sample;
   assign sample = !ss && (!sck_r[1] && sck_r[0]);
   assign shift = !ss && (sck_r[1] && !sck_r[0]);
   
   /* start/end of transmission */
   wire load;
   wire store;
   assign load = ss_r[1] && !ss_r[0];
   assign store = !ss_r[1] && ss_r[0];
   
   assign miso = !ss ? shr_tx[7] : 1'bz; /* TODO: check if works, yosys
					  could not implement this */
   
   /* detect rising and falling edge of sck and ss */
   always @(posedge clk) begin
      sck_r <= {sck_r[0], sck};
      ss_r <= {ss_r[0], ss};
   end
   
   /* miso (tx) */
   always @(posedge clk)
      if (shift)
        shr_tx <= {shr_tx[6:0], 1'b0};
    else if (load)
        shr_tx <= txdata;
      
   /* mosi (rx) */
   always @(posedge clk)
      if (sample)
        shr_rx <= {shr_rx[6:0], mosi};
      else if (store)
	begin
           rxdata <= shr_rx;
	   rxdataavail <= 1'b1;
	end
      else if(load)
	begin
	   rxdataavail <= 1'b0;
	end
endmodule


/* --------------------------------a */

module div2(input in,
	    output out);

   wire 	   in, out;
   reg 		   c;

   always@(posedge in)
     begin
	c<=!c;	
     end

   assign out=c; /* TODO: simplify */
      
endmodule /* # div2 end # */



module div5(input in,
	    output out);

   wire 	   in;
   reg [2:0] 	   c;   
   reg 		   out;
   
   initial
     begin
	c=3'b000;
     end
   
   always@(posedge in)
     begin
	if(c>=4)
	  begin
	     c<=0;
	     out<=1'b1;	     
	  end
	else
	  begin
	     out<=1'b0;
	     c<=c+1;
	  end
     end
   
endmodule /* # div5 end # */

module div3(input in,
	    output out);

   wire 	   in;
   reg [1:0] 	   c;   
   reg 		   out;
   
   initial
     begin
	c=3'b00;
     end
   
   always@(posedge in)
     begin
	if(c>=2)
	  begin
	     c<=0;
	     out<=1'b1;	     
	  end
	else
	  begin
	     out<=1'b0;
	     c<=c+1;
	  end
     end
   
endmodule /* # div5 end # */

module div100(input in,
	      output out);

   wire 	     in;
   reg 		     out;
   reg [6:0] 	     c;

   initial
     begin
	c=6'b0000000;
     end
   
   always@(posedge in)
     begin
	if(c>=99)
	  begin
	     c<=0;
	     out=1'b1;
	  end
	else
	  begin
	     out=1'b0;
	     c<=c+1;
	  end
     end
   
endmodule /* # div100 end # */

#include <stm32f1xx.h>

#include "blink.hpp"
#include "ds1307.hpp"
#include "gpio.hpp"
#include "systick.hpp"
#include "uart.hpp"
#include "utils.h"
#include "version.git.h"

// ONLY FOR SRAM_BOOT
// extern uint32_t g_pfnVectors;

int main() {
  /* system init */
  SystemCoreClockUpdate();

  /* ONLY FOR SRAM_BOOT: relocate ISR table */
  // SCB->VTOR = (uint32_t)&g_pfnVectors;

  /* setup the debugging uart */
  usart1_init(9600);
  /* init on-board led as debug blinker */
  blink_init();
  /* init an rtc as I2C peripheral */
  ds1307_init();

  /* configure the SysTick timer to overflow every 1 us */
  SysTick_Config(SystemCoreClock / 10000);

  __enable_irq();

  /* here begins the main code */
  printf("Hello World!" ENDL ">");

  while (1) {
    int ms_cnt = 0;

    if (usart1_data_available()) {
      char msg[50];
      uint32_t size;

      usart1_get_data(msg, &size);

      printf(ENDL);  // start with clean line

      // Print Version
      if (msg[0] == 'v') {
        printf("TEST " xstr(BRANCH) " " xstr(COMMIT) " " xstr(VERSION) "." xstr(
            MAJOR) "." xstr(MINOR) "-" xstr(DIRTY) ENDL);
      }
      // Echo
      else if (msg[0] == 'e') {
        size--;
        printf("%.*s" ENDL, (int)size, msg + 1);
      }
      // Write a timestamp in RTC
      else if (msg[0] == 's') {
        ds_datetime_t dt = {0, 20, 10, 1, 11, 12, 20};
        ds_set_date(&dt);
        printf("SET" ENDL);
      }
      // Get timestamp from RTC
      else if (msg[0] == 'g') {
        ds_datetime_t dt;
        ds_get_date(&dt);
        hexdump("GOT: " ENDL, sizeof(dt), (uint8_t*)&dt);
      }

      printf("> ");
    }

    // raw and ugly heartbeat
    _delay_ms(1);
    if (ms_cnt == 500) {
      blink();
      ms_cnt = 0;
    }
  }
}
/**
 * @file startup_stm32f103xb.h
 * @author Giulio (giulio@intimelink.com)
 * @brief Why this? cxx can't override weak handlers in startup_xxx.s
 * @version 0.1
 * @date 2020-11-29
 * 
 * @copyright MIT License (c) 2020
 * 
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void Reset_Handler(void) __attribute__((weak));
void NMI_Handler(void) __attribute__((weak));
void HardFault_Handler(void) __attribute__((weak));
void MemManage_Handler(void) __attribute__((weak));
void BusFault_Handler(void) __attribute__((weak));
void UsageFault_Handler(void) __attribute__((weak));
void SVC_Handler(void) __attribute__((weak));
void DebugMon_Handler(void) __attribute__((weak));
void PendSV_Handler(void) __attribute__((weak));
void SysTick_Handler(void) __attribute__((weak));
void WWDG_IRQHandler(void) __attribute__((weak));
void PVD_IRQHandler(void) __attribute__((weak));
void TAMPER_IRQHandler(void) __attribute__((weak));
void RTC_IRQHandler(void) __attribute__((weak));
void FLASH_IRQHandler(void) __attribute__((weak));
void RCC_IRQHandler(void) __attribute__((weak));
void EXTI0_IRQHandler(void) __attribute__((weak));
void EXTI1_IRQHandler(void) __attribute__((weak));
void EXTI2_IRQHandler(void) __attribute__((weak));
void EXTI3_IRQHandler(void) __attribute__((weak));
void EXTI4_IRQHandler(void) __attribute__((weak));
void DMA1_Channel1_IRQHandler(void) __attribute__((weak));
void DMA1_Channel2_IRQHandler(void) __attribute__((weak));
void DMA1_Channel3_IRQHandler(void) __attribute__((weak));
void DMA1_Channel4_IRQHandler(void) __attribute__((weak));
void DMA1_Channel5_IRQHandler(void) __attribute__((weak));
void DMA1_Channel6_IRQHandler(void) __attribute__((weak));
void DMA1_Channel7_IRQHandler(void) __attribute__((weak));
void ADC1_2_IRQHandler(void) __attribute__((weak));
void USB_HP_CAN1_TX_IRQHandler(void) __attribute__((weak));
void USB_LP_CAN1_RX0_IRQHandler(void) __attribute__((weak));
void CAN1_RX1_IRQHandler(void) __attribute__((weak));
void CAN1_SCE_IRQHandler(void) __attribute__((weak));
void EXTI9_5_IRQHandler(void) __attribute__((weak));
void TIM1_BRK_IRQHandler(void) __attribute__((weak));
void TIM1_UP_IRQHandler(void) __attribute__((weak));
void TIM1_TRG_COM_IRQHandler(void) __attribute__((weak));
void TIM1_CC_IRQHandler(void) __attribute__((weak));
void TIM2_IRQHandler(void) __attribute__((weak));
void TIM3_IRQHandler(void) __attribute__((weak));
void TIM4_IRQHandler(void) __attribute__((weak));
void I2C1_EV_IRQHandler(void) __attribute__((weak));
void I2C1_ER_IRQHandler(void) __attribute__((weak));
void I2C2_EV_IRQHandler(void) __attribute__((weak));
void I2C2_ER_IRQHandler(void) __attribute__((weak));
void SPI1_IRQHandler(void) __attribute__((weak));
void SPI2_IRQHandler(void) __attribute__((weak));
void USART1_IRQHandler(void) __attribute__((weak));
void USART2_IRQHandler(void) __attribute__((weak));
void USART3_IRQHandler(void) __attribute__((weak));
void EXTI15_10_IRQHandler(void) __attribute__((weak));
void RTC_Alarm_IRQHandler(void) __attribute__((weak));
void USBWakeUp_IRQHandler(void) __attribute__((weak));

#ifdef __cplusplus
}
#endif
/**
 * @file systick.cpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief 
 * @version 0.1
 * @date 2020-12-08
 * 
 * @copyright xxxx (x) 2020
 * 
 */

#include "systick.hpp"

#include <stm32f1xx.h>

static volatile uint32_t us_ticks = 0;

// SysTick_Handler function will be called every 1 us
void SysTick_Handler(void) {
  if (us_ticks != 0) {
    us_ticks--;
  }
}

void _delay_us(uint32_t us) {
  // Reload us value
  us_ticks = us;
  // Wait until usTick reach zero
  while (us_ticks)
    ;
}

void _delay_ms(uint32_t ms) {
  // Wait until ms reach zero
  while (ms--) {
    // Delay 1ms
    _delay_us(10);
  }
}
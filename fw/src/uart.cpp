/**
 * @file uart.cpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief xxx
 * @version xxx
 * @date xxx
 *
 * @copyright xxxx 2020
 *
 */

/*================================= Includes =================================*/
#include "uart.hpp"
#include "gpio.hpp"

#include <stdarg.h>
#include <stm32f1xx.h>
#include <string.h>

/*========================== Macros and Definitions ==========================*/
#define RXBUFFLEN 50

/*===================== Local data structures and types  =====================*/
struct status_t {
  uint32_t completed :1;
  uint32_t :0;
};

/*=========================== Function prototypes  ===========================*/
static inline void usart1_clrbuf();
static inline bool usart1_pushrx(const char c);

static void usart1_putc(const char c);
static void usart1_puts(const char* b);

/*============================= Extern variables =============================*/

/*==================== Constant and Variable Definitions  ====================*/
volatile uint8_t usart1_rxbuff[RXBUFFLEN];
volatile uint8_t usart1_rxsize = 0;
static status_t usart1_rxstatus = {};

/*======================== Implementation begins here ========================*/

/* * * * * * * * * * * * Functions for buffer handling  * * * * * * * * * * * */

/**
 * @brief Clear UART buffer
 */
static inline void usart1_clrbuf() {
  usart1_rxsize = 0;
}

/**
 * @brief Insert data in UART buffer
 * 
 * @param c 
 * @return false if fails, i.e. buffer full
 */
static inline bool usart1_pushrx(const char c) {
  if (usart1_rxsize >= sizeof(usart1_rxbuff) - 1)
    return false;

  usart1_rxbuff[usart1_rxsize++] = c;
  return true;
}

/* * * * * * * * * * * * Functions for USART1 handling  * * * * * * * * * * * */

/**
 * @brief Configure and enable USART1 peripheral
 * 
 * @param baudrate 
 */
void usart1_init(const uint32_t baudrate) {
  NVIC_DisableIRQ(USART1_IRQn);

  // TODO
  /* Peripheral reset control to UART, a "1" bring it out of reset. */
  // RCC->APB2RSTR &= ~RCC_APB2RSTR_USART1RST;
  // RCC->APB2RSTR |= RCC_APB2RSTR_USART1RST;

  /* Enable UART clock */
  RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

  /* Hardcoded USART configuration (8N1, okay for debug) */
  USART1->CR1 = USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE;
  USART1->CR2 = 0x00;
  USART1->CR3 = 0x00;

  /* Baudrate setup */
  USART1->BRR = (SystemCoreClock / baudrate);

  /* Configure RX/TX GPIO */
  RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
  uint32_t cr = GPIOA->CRH;
  GPIO_SET_CRH(cr, 9, GPIO_MODE_OUTPUT_2, GPIO_CNF_AF_TOTEM_POLE);
  GPIO_SET_CRH(cr, 10, GPIO_MODE_INPUT, GPIO_CNF_FLOATING);
  GPIOA->CRH = cr;

  /* enable usart1 irq */
  NVIC_EnableIRQ(USART1_IRQn);

  /* enable usart1*/
  USART1->CR1 |= USART_CR1_UE;
}

/**
 * @brief ISR routine, put a whole line in buffer. If size exceeded, truncate.
 * 
 */
void USART1_IRQHandler(void) {
  uint16_t status = USART1->SR;

  /* check if data received */
  if (status & USART_SR_RXNE) {
    /* this clears the RXNE flag */
    char c = USART1->DR;
    
    // Do not mess up unread data
    if (usart1_rxstatus.completed)
      return;

    if (c == '\n' || c == '\r') {
      if (usart1_rxsize == 0)
        return;
      usart1_rxstatus.completed = true;
    } else {
      usart1_pushrx(c);
      // Echo enabled
      usart1_putc(c);
    }
  }
}

/* * * * * * * * High level interfacing functions (with stdlib) * * * * * * * */

static void usart1_putc(const char c) {
  while ((USART1->SR & USART_SR_TXE) == 0)
    ;
  USART1->DR = c;
}

static void usart1_puts(const char* b) {
  for (uint16_t i = 0; b[i] != '\0'; i++)
    usart1_putc(b[i]);
}

int printf(const char* __fmt, ...) {
  int ret;
  char buf[RXBUFFLEN];
  va_list lst;
  va_start(lst, __fmt);
  ret = vsnprintf(buf, sizeof(buf), __fmt, lst);
  usart1_puts(buf);
  va_end(lst);
  return ret;
}

/**
 * @brief print raw data in hexadecimal, with trailing newline
 * 
 * @param str Leading string, separator is not necessary, can be NULL or empty
 * @param size data size
 * @param data data pointer
 */
void hexdump(const char* str, const uint8_t size, const uint8_t* data) {
  usart1_puts(str);
  for (uint8_t i = 0; i < size; i++)
    printf(" %x", data[i]);
  printf(ENDL);
}

bool usart1_data_available() {
  return usart1_rxstatus.completed;
}

void usart1_get_data(char* data, uint32_t* size) {
  *size = usart1_rxsize;
  memcpy(data, (void*)usart1_rxbuff, *size);
  usart1_rxstatus.completed = false;
  usart1_clrbuf();
}
/**
 * @file utils.h
 * @author Giulio (giulio@glgprograms.it)
 * @brief I2C master configuration
 * @version 0.1
 * @date 2020-12-08
 *
 * @copyright xxxx (x) 2020
 *
 */

#pragma once

// TODO: concatenation

// Stringification
#define xstr(s) str(s)
#define str(s) #s
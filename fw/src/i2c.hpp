/**
 * @file i2c.hpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief I2C master configuration
 * @version 0.1
 * @date 2020-12-08
 *
 * @copyright xxxx (x) 2020
 *
 */

#pragma once

void I2C_MasterInit();
void I2C_MstSendRcv(const uint32_t addr,
                    const uint8_t* tx,
                    const uint32_t txlen,
                    uint8_t* rx,
                    uint32_t rxlen);

static inline void I2C_MstSend(const uint32_t addr,
                               const uint8_t* tx,
                               const uint32_t txlen) {
  I2C_MstSendRcv(addr, tx, txlen, nullptr, 0);
}

/**
 * @file systick.hpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief 
 * @version 0.1
 * @date 2020-12-08
 * 
 * @copyright xxxx (x) 2020
 * 
 */

#include <stdint.h>

void _delay_us(uint32_t us);
void _delay_ms(uint32_t ms);
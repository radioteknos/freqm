/**
 * @file ds1307.cpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief Example of I2C configuration applied to RTC
 * @version 0.1
 * @date 2020-12-08
 * 
 * @copyright xxxx (x) 2020
 * 
 */

#include "ds1307.hpp"
#include <stm32f1xx.h>
#include <string.h>
#include "i2c.hpp"

// 7 bit address + rw
#define SLAVE_ADDRESS 0b11010000

/**
 * @brief Initialise the RTC
 * 
 * @return true if correctly initialised 
 */
bool ds1307_init() {
    I2C_MasterInit();
    return true;
}

/**
 * @brief Fetch current timestamp from RTC
 * 
 * @param datetime Pointer to timestamp structure
 * @return true if correctly received
 */
bool ds_get_date(ds_datetime_t* datetime) {
  uint8_t ptr = 0x00;
  I2C_MstSendRcv(SLAVE_ADDRESS, &ptr, 1, (uint8_t*)datetime, sizeof(*datetime));
  return 0;
}

/**
 * @brief Set timestamp in RTC
 * 
 * @param datetime Pointer to timestamp structure
 * @return true if correctly transmitted
 */
bool ds_set_date(const ds_datetime_t* datetime) {
  uint8_t ptr[sizeof(ds_datetime_t) + 1];
  ptr[0] = 0;
  memcpy(ptr + 1, datetime, sizeof(ds_datetime_t));
  I2C_MstSend(SLAVE_ADDRESS, ptr, sizeof(ptr));
  return 0;
}
/**
 * @file ds1307.hpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief Example of I2C configuration applied to RTC (plus C++ style datetime
 * structures)
 * @version 0.1
 * @date 2020-12-08
 *
 * @copyright xxxx (x) 2020
 *
 */

#pragma once

#include <stm32f1xx.h>
#include <string.h>
#include "i2c.hpp"

// 7 bit address + rw
#define SLAVE_ADDRESS 0b11010000

/*======================== Data structures and types  ========================*/

struct ds_seconds_t {
 public:
  ds_seconds_t() = default;
  ds_seconds_t(uint8_t);

 public:
  uint8_t seconds_u : 4;
  uint8_t seconds_d : 3;
  uint8_t ch : 1;
} __PACKED;

struct ds_minutes_t {
 public:
  ds_minutes_t() = default;
  ds_minutes_t(uint8_t);

 public:
  uint8_t minutes_u : 4;
  uint8_t minutes_d : 3;
} __PACKED;

struct ds_hours_t {
 public:
  ds_hours_t() = default;
  ds_hours_t(uint8_t);

 public:
  uint8_t hours_u : 4;
  uint8_t hours_d : 2;
  uint8_t fmt12 : 1;
} __PACKED;

struct ds_dow_t {
 public:
  ds_dow_t() = default;
  ds_dow_t(uint8_t);

 public:
  uint8_t dow : 3;
} __PACKED;

struct ds_date_t {
 public:
  ds_date_t() = default;
  ds_date_t(uint8_t);

 public:
  uint8_t date_u : 4;
  uint8_t date_d : 2;
} __PACKED;

struct ds_month_t {
 public:
  ds_month_t() = default;
  ds_month_t(uint8_t);

 public:
  uint8_t month_u : 4;
  uint8_t month_d : 1;
} __PACKED;

struct ds_year_t {
 public:
  ds_year_t() = default;
  ds_year_t(uint8_t);

 public:
  uint8_t year_u : 4;
  uint8_t year_d : 4;
} __PACKED;

struct ds_control_t {
  uint8_t rs0 : 1;
  uint8_t rs1 : 1;
  uint8_t : 2;
  uint8_t sqwe : 1;
  uint8_t : 2;
  uint8_t out : 1;
} __PACKED;

class ds_datetime_t {
 public:
  ds_datetime_t() = default;

 public:
  ds_seconds_t seconds;
  ds_minutes_t minutes;
  ds_hours_t hours;
  ds_dow_t dow;
  ds_date_t date;
  ds_month_t month;
  ds_year_t year;
} __PACKED;

/*=========================== Function prototypes  ===========================*/

bool ds1307_init();
bool ds_get_date(ds_datetime_t* datetime);
bool ds_set_date(const ds_datetime_t* datetime);

/*======================== Implementation begins here ========================*/

inline ds_seconds_t::ds_seconds_t(uint8_t v) {
  this->seconds_u = v % 10;
  this->seconds_d = v / 10;
  // TODO: handle CH better
  this->ch = 0;
}

inline ds_minutes_t::ds_minutes_t(uint8_t v) {
  this->minutes_u = v % 10;
  this->minutes_d = v / 10;
}

// TODO: handle 12/24
inline ds_hours_t::ds_hours_t(uint8_t v) {
  this->hours_u = v % 10;
  this->hours_d = v / 10;
}

inline ds_dow_t::ds_dow_t(uint8_t v) {
  this->dow = v;
}

inline ds_date_t::ds_date_t(uint8_t v) {
  this->date_u = v % 10;
  this->date_d = v / 10;
}

inline ds_month_t::ds_month_t(uint8_t v) {
  this->month_u = v % 10;
  this->month_d = v / 10;
}

inline ds_year_t::ds_year_t(uint8_t v) {
  this->year_u = v % 10;
  this->year_d = v / 10;
}
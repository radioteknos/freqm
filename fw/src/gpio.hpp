/**
 * @file gpio.hpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief Some macros for GPIO configuration
 * @version 0.1
 * @date 2020-12-08
 *
 * @copyright xxxx (x) 2020
 *
 */

#pragma once
#include <stm32f1xx.h>

/**
 * @brief GPIO cnf for input pin.
 */
enum GpioCrCnfIn {
  GPIO_CNF_ANALOG = 0b00, /** Input as analog */
  GPIO_CNF_FLOATING = 0b01, /** Input as floating */
  GPIO_CNF_PUPD = 0b10 /** Input with pullup */
};

/**
 * @brief GPIO cnf for output pin.
 */
enum GpioCrCnfOut {
  GPIO_CNF_TOTEM_POLE = 0b00, /** Output as totem pole (push pull) */
  GPIO_CNF_OPEN_DRAIN = 0b01, /** Output as open drain */
  GPIO_CNF_AF_TOTEM_POLE = 0b10, /** Output as alternate function totem pole */
  GPIO_CNF_AF_OPEN_DRAIN = 0b11 /** Output as alternate function open drain */
};

/**
 * @brief GPIO mode configuration
 */
enum GpioCrMode {
  GPIO_MODE_INPUT = 0b00,
  GPIO_MODE_OUTPUT_10 = 0b01,
  GPIO_MODE_OUTPUT_2 = 0b10,
  GPIO_MODE_OUTPUT_50 = 0b11,
};

/**
 * @brief Macro for gpio pin setup (low register, i.e. pin 0-7)
 */
#define GPIO_SET_CRL(CRL, pin, mode, cnf)        \
  {                                              \
    static_assert((pin) < 8, "CRL has pin < 8"); \
    CRL &= ~(0x0f << ((pin)*4));                 \
    CRL |= ((mode) | ((cnf) << 2)) << ((pin)*4); \
  }

/**
 * @brief Macro for gpio pin setup (high register, i.e. pin 8-15)
 */
#define GPIO_SET_CRH(CRH, pin, mode, cnf)              \
  {                                                    \
    static_assert((pin) > 8, "CRH has pin > 8");       \
    CRH &= ~(0x0f << ((pin - 8) * 4));                 \
    CRH |= ((mode) | ((cnf) << 2)) << ((pin - 8) * 4); \
  }

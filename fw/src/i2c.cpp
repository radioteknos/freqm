/**
 * @file i2c.cpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief I2C master configuration
 * @version 0.1
 * @date 2020-12-08
 *
 * @copyright xxxx (x) 2020
 *
 */

#include <stm32f1xx.h>
#include "gpio.hpp"

/**
 * @brief Initialise the registers for I2C Master
 */
void I2C_MasterInit() {
  /* enable clock domain */
  RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
  RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;

  /* Disable the interface */
  I2C1->CR1 = 0x00;

  /* Configure the GPIO pin */
  uint32_t crl = GPIOB->CRL;
  GPIO_SET_CRL(crl, 6, GPIO_MODE_OUTPUT_2, GPIO_CNF_AF_OPEN_DRAIN);
  GPIO_SET_CRL(crl, 7, GPIO_MODE_OUTPUT_2, GPIO_CNF_AF_OPEN_DRAIN);
  GPIOB->CRL = crl;

  /* Program the peripheral input clock in order to generate correct timings.
     No interrupts, minimum frequency */
  I2C1->CR2 = I2C_CR2_FREQ_1;

  /* Configure the clock control register (half period!)
     Standard Mode (Sm), 100kHz clock (2MHz/(2*10)) */
  I2C1->CCR = 10;

  /* Configure the rise time register (1us) */
  I2C1->TRISE = 2;

  /* Enable the peripheral */
  I2C1->CR1 |= I2C_CR1_PE;

  /* Set ack and clear pos for first transmission */
  I2C1->CR1 |= I2C_CR1_ACK;
  I2C1->CR1 &= ~I2C_CR1_POS;
}

/**
 * @brief Send chunk of data and immediately receive another chunk
 * 
 * @param addr I2C address
 * @param tx pointer to transmitted data buffer
 * @param txlen length of transmitted data buffer
 * @param rx pointer to received data buffer
 * @param rxlen length of received data buffer
 */
void I2C_MstSendRcv(const uint32_t addr,
                    const uint8_t* tx,
                    const uint32_t txlen,
                    uint8_t* rx,
                    const uint32_t rxlen) {
  /* Set the start bit */
  I2C1->CR1 |= I2C_CR1_START;

  /* Wait for start bit send */
  while (!(I2C1->SR1 & I2C_SR1_SB))
    ;

  I2C1->DR = addr & ~0x01;

  /* wait for address transmission */
  while (!(I2C1->SR1 & I2C_SR1_ADDR))
    ;
  /* this clears ADDR */
  (void)I2C1->SR2;

  /* transmit the whole buffer */
  for (uint32_t i = 0; i < txlen; i++) {
    /* Move only if TXRDY is ready */
    // while (!(I2C1->SR1 & I2C_SR1_TXE))
    // ;
    I2C1->DR = tx[i];
    while (!(I2C1->SR1 & I2C_SR1_BTF))
      ;
  }

  if (rxlen == 0) {
    /* send stop and return */
    I2C1->CR1 |= I2C_CR1_STOP;
    /* Wait until STOP is cleared by hardware */
    while (I2C1->CR1 & I2C_CR1_STOP)
      ;
    return;
  }

  /* Wait for the last TX to finish before setting repeated start. */
  /* Move on only if TXRDY is ready */
  // TODO:?

  /* Repeated Start */
  I2C1->CR1 |= I2C_CR1_START;

  /* Wait for start bit send */
  while (!(I2C1->SR1 & I2C_SR1_SB))
    ;

  I2C1->DR = addr | 0x01;

  /* Wait until ADDR is set */
  while (!(I2C1->SR1 & I2C_SR1_ADDR))
    ;

  uint32_t i = 0;
  if (rxlen > 2) {
    /* Clear ADDR */
    (void)I2C1->SR2;

    for (; i < rxlen - 2; i++) {
      /* Wait until BTF is set */
      while (!(I2C1->SR1 & I2C_SR1_BTF))
        ;

      rx[i] = I2C1->DR;
    }

    /* Read the remaining two bytes... */

    /* Wait until BTF is set */
    while (!(I2C1->SR1 & I2C_SR1_BTF))
      ;
    /* Clear ACK bit */
    I2C1->CR1 &= ~I2C_CR1_ACK;
    __disable_irq();
    /* Set stop bit */
    I2C1->CR1 |= I2C_CR1_STOP;
    rx[i++] = I2C1->DR;
    __enable_irq();
    /* Wait until RXNE is set */
    while (!(I2C1->SR1 & I2C_SR1_RXNE))
      ;
    rx[i++] = I2C1->DR;

    /* Wait until STOP is cleared by hardware */
    while (I2C1->CR1 & I2C_CR1_STOP)
      ;

    /* Set ack for next transmission */
    I2C1->CR1 |= I2C_CR1_ACK;
  } else if (rxlen == 2) {
    /* Set POS (position of ack bit) */
    I2C1->CR1 |= I2C_CR1_POS;

    /* disable interrupts */
    /* Clear ADDR */
    (void)I2C1->SR2;

    /* Clear ACK */
    I2C1->CR1 &= ~I2C_CR1_ACK;
    /* enable interrupts */

    /* Wait until BTF is set */
    while (!(I2C1->SR1 & I2C_SR1_BTF))
      ;

    __disable_irq();
    /* Set STOP */
    I2C1->CR1 |= I2C_CR1_STOP;

    rx[i++] = I2C1->DR;
    __enable_irq();

    rx[i++] = I2C1->DR;

    /* Wait until STOP is cleared by hardware */
    while (I2C1->CR1 & I2C_CR1_STOP)
      ;

    /* Set ack and clear pos for next transmission */
    I2C1->CR1 |= I2C_CR1_ACK;
    I2C1->CR1 &= ~I2C_CR1_POS;
  } else {
    /* Clear ACK */
    I2C1->CR1 &= ~I2C_CR1_ACK;

    __disable_irq();
    /* Clear ADDR */
    (void)I2C1->SR2;

    /* Set STOP */
    I2C1->CR1 |= I2C_CR1_STOP;
    __enable_irq();

    /* Wait RXNE */
    while (!(I2C1->SR1 & I2C_SR1_RXNE))
      ;

    rx[i++] = I2C1->DR;

    /* Wait until STOP is cleared by hardware */
    while (I2C1->CR1 & I2C_CR1_STOP)
      ;

    /* Set ack for next transmission */
    I2C1->CR1 |= I2C_CR1_ACK;
  }

  /* Should check for idle? */
}
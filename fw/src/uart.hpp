#pragma once

#include <stdio.h>
#include <stdint.h>

// Newline preset (default LFCR)
#define ENDL "\n\r"

void usart1_init(const uint32_t baudrate);
bool usart1_data_available();
void usart1_get_data(char* data, uint32_t* size);
void hexdump(const char* str, const uint8_t size, const uint8_t* data);
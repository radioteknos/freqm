/**
 * @file blink.hpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief Example of GPIO configuration applied to blinking LED
 * @version 0.1
 * @date 2020-12-08
 * 
 * @copyright xxxx (x) 2020
 * 
 */

#pragma once

void blink_init();
void blink();
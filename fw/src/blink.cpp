/**
 * @file blink.cpp
 * @author Giulio (giulio@glgprograms.it)
 * @brief Example of GPIO configuration applied to blinking LED
 * @version 0.1
 * @date 2020-12-08
 * 
 * @copyright xxxx (x) 2020
 * 
 */

#include "blink.hpp"
#include "gpio.hpp"

#include <stm32f1xx.h>

void blink_init() {
  /* Enable clock domain for PORT C */
  RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
  uint32_t cr = GPIOC->CRH;
  GPIO_SET_CRH(cr, 13, GPIO_MODE_OUTPUT_2, GPIO_CNF_TOTEM_POLE);
  GPIOC->CRH = cr;
}

void blink() {
  static bool on = false;
  if (on)
    GPIOC->ODR |= GPIO_ODR_ODR13;
  else
    GPIOC->ODR &= ~GPIO_ODR_ODR13;
  on ^= 1;
}
# Alcune note

## Compilazione

1. Generare o creare manualmente il file di versione `src/version.git.h`

        #pragma once
        #define BRANCH xxxxx
        #define COMMIT xxxxx
        #define VERSION 0
        #define MAJOR 0
        #define MINOR 0
        #define DIRTY dirty

1. Lanciare `make`

## Caricamento

### Con ST-Link
Decommentare la sezione `## With ST-Link` nel makefile, poi

        make flash

### Con stm32flash (bootloader)
Decommentare la sezione `## With stm32flash` nel makefile, poi

        make flash PRGPORT=/dev/ttyUSBxxx

Ricordarsi di spostare i jumper BOOT1 e BOOT0in posizione 01 "system memory" per settare l'entrypoint al bootloader hardware (pag 60 RM0008).

## Boot da SRAM
1. Decommentare le righe `ONLY FOR SRAM_BOOT` nel `main.cpp` e la riga che imposta `STM32F103C8Tx_SRAM.ld` come linker file nel makefile.
1. (Ri)compilare
1. Impostare i jumper di boot in posizione 11 (Embedded SRAM)

## Debugging con ST-Link
1. Requisiti: openocd, arm-none-eabi-gdb
1. Creare una copia di /usr/share/openocd/scripts/target/stm32f1x.cfg (es. stm32f1x2.cfg) ed aggiungere la riga `set CPUTAPID 0` (alcuni cloni ST falliscono nella presentazione dell'ID)
1. Lanciare openocd

        openocd -f "interface/stlink-v2.cfg" -c "transport select hla_swd" -f "target/stm32f1x2.cfg"
1. Avviare gdb

        arm-none-eabi-gdb output/arm-test.elf -ex "target remote 127.0.0.1:3333"
